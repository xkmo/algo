/**
	Export: CashCow [Object]
		Property: strategies, exchanges
		Class: Bot(strategyName, config),
*/
_ = Npm.require('lodash');
klass = Npm.require('klass');

CashCow = {};

Meteor.startup(function() {
	CashCow.exchanges = [
		{
			_id: 'okcoincom',
			name:'OKCoin Int\'l',
			homepage: 'https://www.okcoin.com',
			tradeTypes: [
				{type: 'spot', name: 'Spot', pairs: [{base: "BTC", counter:"USD"}, {base:"LTC", counter:"USD"}], leverage: true},
				{type: 'future', name:'Future', pairs: [{base: "BTC", counter:"USD"}, {base:"LTC", counter:"USD"}], contracts: ['this_week', 'next_week', 'quarter']}
			]
		},
		{
			_id: 'okcoincn',
			name:'OKCoin China',
			homepage: 'https://www.okcoin.cn',
			tradeTypes: [
				{type: 'spot', name: 'Spot', pairs:[{base: "BTC", counter:"CNY"}, {base:"LTC", counter:"CNY"}], leverage: true}
			]
		},
		{
			_id: 'bitstamp',
			name:'Bitstamp',
			homepage: 'https://www.bitstamp.net',
			tradeTypes: [
				{type: 'spot', name: 'Spot', pairs:[{base: "BTC", counter:"USD"}], leverage: false}
			]
		}
		// {name: 'huobi', displayName:'Huobi', homepage: 'https://www.huobi.com', pairs:[{base: "BTC", counter:"CNY"}, {base:"LTC", counter:"CNY"}]}
	];
	CashCow.strategies = [
		{_id: 'scalping', name:'Scalping'},
		{_id: 'spot_arbitrage', name:'Spot Arbitrage'}
	];

	CashCow.manager = new Manager();
});
