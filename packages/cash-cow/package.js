Package.describe({
  name: 'cash-cow',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.4.2');
  api.export('CashCow', 'server');

  api.use('http', 'server');

  var exchanges_dir = 'exchanges';
  var strategies_dir = 'strategies';
  api.addFiles(['index.js', 'manager.js', 'bot.js', 'fx.js'], 'server');
  api.addFiles([exchanges_dir+'/exchange.js', exchanges_dir+'/okcoin.js', exchanges_dir+'/bitstamp.js'], 'server');
  api.addFiles([strategies_dir+'/scalping.js', strategies_dir+'/spot_arbitrage.js'], 'server', {isAsset: true});
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('cash-cow');
  api.addFiles('cash-cow-tests.js');
});

Npm.depends({
  'lodash': '3.5.0',
  "klass": "1.4.0",
  'ws': '0.7.1',
  'MD5': '1.2.1',
  'bitstamp': '0.1.9',
  'bitstamp-ws': 'https://github.com/askmike/bitstamp-ws/tarball/d1417ac3fd12fe55f76be2393cc0bd694a08de0c',
  'okcoin': '0.0.1'
});
