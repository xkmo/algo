var resources = null;
function getResources() {
	var url = 'http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json';
	HTTP.get(url, function(err, result) {
		if (!err) resources = result.data.list.resources;
	});
}

fx = function(name) {
	var resource = _.find(resources, function(n) {
		return n.resource.fields.name == name;
	});

	if (!resource) {
		return 0;
	}

	return resource.resource.fields.price;
}

Meteor.startup(function() {
	setInterval(getResources, 10*60*1000);
	getResources();
});
