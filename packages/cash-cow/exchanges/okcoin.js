var WebSocket = Npm.require('ws');
var md5 = Npm.require('MD5');
var OKCoinREST = Npm.require('okcoin');

OKCoin = Exchange.extend({
	site: null,
	initialize: function (site, ready) {
		var self = this;

		self.site = site;
		var ws = new WebSocket('wss://real.okcoin.'+site+':10440/websocket/okcoinapi');
		ws.on('open', function() {
			ready();
			console.log('OKCoin'+site+' connected at: ' + new Date());
		});

		ws.on('message', function(message) {
			// depth data
			message = JSON.parse(message)[0];
			_(message.data.asks).reverse().value(); // reverse the asks array's order

			_.forEach(self.bots[message.channel], function(b) {
				b.handleData(message.data, self.id);
			});
		});

		this.ws = ws;
	},

	addChannel: function(config, bot) {
		// config object: {tradePair: "BTC-USD", tradeType: "future"}
		var channel = null;
		switch (this.site) {
			case 'com':
				if (config.tradeType === 'future') {
					// OKCoinCom channel format: ok_btcusd_future_ticker_this_week
					var contract = 'this_week';
					channel = 'ok_' + config.tradePair.split('-').join('').toLowerCase() + '_future_depth_' + contract;
				}
				break;
			case 'cn':
				if (config.tradeType === 'spot') {
					channel = 'ok_'+config.tradePair.split('-').join('').toLowerCase()+'_depth';
				}
				break;
		}

		var bots = this.bots[channel];
		if (!bots) {
			bots = [];
			this.bots[channel] = bots;

			try {
				this.ws.send(JSON.stringify({'event': 'addChannel','channel': channel}));
			} catch (e) {
				console.log('ws.send addChannel', e);
			}
		}

		bots.push(bot);
	},

	getAccount: function(ex, bot) {
		var self = this;

		var rest = new OKCoinREST(ex.apiKey, ex.apiSecret, 'https://www.okcoin.'+this.site);
		rest.getUserInfo(function(err, result) {
			if (!err) {
				var account = {
					free: result.info.funds.free
				}
				bot.context.accounts[self.id] = account;
				console.log(self.id+' account: ', account);
			}
		});
	}
});
