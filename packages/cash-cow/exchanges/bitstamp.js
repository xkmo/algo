var BitstampWS = Npm.require('bitstamp-ws');
var BitstampREST = Npm.require('bitstamp');

Bitstamp = Exchange.extend({
	initialize: function (ready) {
		var ws = new BitstampWS({
      order_book: true
    });
		ws.client.on('connect', function() {
			ready();
			console.log('Bitstamp connected at: ' + new Date());
		});

		this.ws = ws;
	},

	addChannel: function(config, bot) {
		var self = this;
		var channel = 'order_book';

		var bots = self.bots[channel];
		if (!bots) {
			bots = [];
			self.bots[channel] = bots;
			self.ws.on(channel, function(data) {
			 	var rate = fx(config.tradePair.split('-')[1]+'/CNY');
				if (!rate) return; // stop when no exchange rate
				var convert = function(order) {
					order[0] = (order[0]*rate).toFixed(2);
				}
				_.forEach(data.bids, convert);
				_.forEach(data.asks, convert);

				data.timestamp = Date.now();

				_.forEach(self.bots[channel], function(b) {
					b.handleData(data, self.id);
				});
			});
		}
		bots.push(bot);

		// var apiHash = self.hash(config.apiKey+config.apiSecret);
		// var account = self.accounts[apiHash];
		// if (!account) {
		// 	self.accounts[apiHash] = {};
		// }
	},

	getAccount: function(ex, bot) {
		var self = this;

		var rest = new BitstampREST(ex.apiKey, ex.apiSecret, ex.clientId);
		rest.balance(function(err, result) {
			if (!err) {
				var account = {
					free: {btc: result['btc_available'], usd: result['usd_available']}
				}
				bot.context.accounts[self.id] = account;
				console.log(self.id+' account: ', account);
			}
		});
	}
});
