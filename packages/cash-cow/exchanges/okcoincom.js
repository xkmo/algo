var WebSocket = Npm.require('ws');
var md5 = Npm.require('MD5');

OKCoinCom = klass({
	ws: null,
	channels: {},

	initialize: function (ready) {
		var self = this;

		var ws = new WebSocket('wss://real.okcoin.com:10440/websocket/okcoinapi');
		ws.on('open', function() {
			ready();
			console.log('OKCoinCom connected at: ' + new Date());
		});

		ws.on('message', function(data) {
			console.log(data);
		});

		this.ws = ws;
	},

	addChannel: function(config, bot) {
		// config object: {tradePair: "BTC-USD", tradeType: "future"}
		var channel = null;
		if (config.tradeType === 'future') {
			// OKCoinCom channel format: ok_btcusd_future_ticker_this_week
			var contract = 'this_week';
			channel = 'ok_' + config.tradePair.split('-').join('').toLowerCase() + '_future_depth_' + contract;
		}

		var bots = this.channels[channel];
		if (!bots) {
			bots = [];
			this.channels[channel] = bots;

			// try {
			// 	this.ws.send(JSON.stringify({'event': 'addChannel','channel': channel}));
			// } catch (e) {
			// 	console.log('ws.send addChannel', e);
			// }
		}

		bots.push(bot);
	}
});
