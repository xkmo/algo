Bot = klass({
	config: null,
	exchanges: {},
	context: {accounts:{}}, // accounts(Object), status(Number)
	depths: {},

	initialize: function(config) {
		this.config = config;

		// function or variable exposed to strategy environment
		function topOrder(depth) {
			return {
				bid: _.first(depth['bids']),
				ask: _.first(depth['asks'])
			};
		}

		function bidTotalHigher(depth, price) {
			var total = 0;
			_(depth['bids']).forEach(function(bid) {
				if (bid[0] >= price) {
					total += bid[1];
				}
				else {
					return false; // exit iteration
				}
			});

			return total;
		}

		function askTotalLower(depth, price) {
			var total = 0;

			_(depth['asks']).forEachRight(function(ask) {
				if (ask[0] <= price) {
					total += ask[1];
				}
				else {
					return false; // exit iteration
				}
			});

			return total;
		}

		eval(Assets.getText('strategies/' + config.strategy + '.js'));

		// init context object

		var self = this;
		this.handleData = function(data, exchangeId) {
			self.depths[exchangeId] = data;

			if (self.context.status) return;

			// validate data and account
			var isValid = true;

			var count = _.keys(self.exchanges).length;
			if (_.keys(self.depths).length === count) {
				var dataExpirationTime = 1000;
				var now = Date.now();
				_.forEach(self.depths, function(n, key) {
					if (now - n.timestamp > dataExpirationTime) {
						isValid = false;
						return false;
					}
				});
			}
			else {
				isValid = false;
			}

			if (_.keys(self.context.accounts).length !== count) {
				isValid = false;
			}

			if (!isValid) return;

			handleData(self.context, self.depths);
		}

		initialize(this.context);
	},

	handleData: null // function
});
