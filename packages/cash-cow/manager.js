Manager = klass({
	bots: {},
	exchanges: {},
	strategies: {},

	initialize: function() {

	},

	ready: function(callback) {
		var self = this;

		var count = 0;
		function ready() {
			++count;
			if (count === CashCow.exchanges.length) {
				callback();
			}
		}

		// use this format _(array).forEach... does not work! ??
		_.forEach(CashCow.exchanges, function(n) {
			var exchangeId = n._id;
			var exchange = null;
			switch (exchangeId) {
				case 'okcoincom':
					exchange = new OKCoin('com', ready);
					break;
				case 'okcoincn':
					exchange = new OKCoin('cn', ready);
					break;
				case 'bitstamp':
					exchange = new Bitstamp(ready);
					break;
			}
			exchange.id = exchangeId;
			self.exchanges[exchangeId] = exchange;
		});
	},

	initBot: function(config) {
		var self = this;

		var bot = new Bot(config);
		_.forEach(config.exchanges, function(n) {
			var exchange = self.exchanges[n.exchangeId];
			exchange.addChannel(n, bot);
			exchange.getAccount(n, bot);
			bot.exchanges[n.exchangeId] = exchange;
		});

		self.bots[config._id] = bot;
	},

	initExchange: function (name) {
		var exchange = this.exchanges[name];

		if (!exchange) {
			// exchange = new Exchange(name);
			this.exchanges[name] = exchange;
		}

		return exchange;
	}
});
