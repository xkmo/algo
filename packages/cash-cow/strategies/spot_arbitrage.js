function initialize(context) {

}

function handleData(context, data) {
	var keys = _.keys(data),
			e1 = keys[0],
			e2 = keys[1],
			d1 = data[e1],
			d2 = data[e2],
			o1 = topOrder(d1),
			o2 = topOrder(d2);

	// find opportunity
	var spread = 5;
	var margin;
	if ((margin = (o1.bid[0] - o2.ask[0] - spread) / 2) && (margin >= 0)) {
		// sell on 1 buy on 2
	}
	else if ((margin = (o2.bid[0] - o1.ask[0] - spread) / 2) && (margin >= 0)) {
		// sell on 2 buy on 1
		swap(e1, e2);
		swap(d1, d2);
		swap(o1, o2);
	}
	else {
		return;
	}
	console.log('sell ' + e1 + ' buy ' + e2 + ' spread ' + (margin + spread));

	// calculate trade amount
	var sellPrice = +(o1.bid[0] - margin).toFixed(2);
	var buyPrice = +(o2.ask[0]  + margin).toFixed(2);
	var maxSellAmount = Math.min(context.accounts[e1].free.btc, bidTotalHigher(d1, sellPrice));
	var maxBuyAmount = Math.min(context.accounts[e1].free.cny/buyPrice, askTotalLower(d1, buyPrice));
	var amount = +(Math.min(
		context.accounts[e1].free.btc,
		bidTotalHigher(d1, sellPrice),
		context.accounts[e2].free.cny/buyPrice,
		askTotalLower(d2, buyPrice))
	).toFixed(3);
	if (amount < 0.01) return;

	// trade to take profit

	// function caculateAmount(bidDepth, topBidPrice, askDepth, topAskPrice) {
	// 	var sellPrice = +(topBidPrice - margin).toFixed(2);
	// 	var buyPrice = +(topAskPrice + margin).toFixed(2);
	//
	// 	// => calculate ammount and send it to client (http://stackoverflow.com/questions/2271156/chrome-desktop-notification-example)
	// 	//
	// 	// var amount = +(Math.min(m1.maxSellAmount(p1), m2.maxBuyAmount(p2))).toFixed(3);
	// 	// if (amount < 0.01) return;
	// }

	function swap(a, b) {
		var c;
		c = a;
		a = b;
		b = c;
	}
}
