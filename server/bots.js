// _ = Meteor.npmRequire('lodash');

Meteor.startup(function() {
	var manager = CashCow.manager;
	var readySync = Meteor.wrapAsync(manager.ready, manager);

	readySync(function() {
		var owner = 'zPjnrrBLNtfAbimdQ';
		var okcoincn_api = APIs.findOne({owner: owner, exchangeId: 'okcoincn'});
		var bitstamp_api = APIs.findOne({owner: owner, exchangeId: 'bitstamp'});

		var spotArbitrageConfig = {
			_id: Random.id(),
			strategy: 'spot_arbitrage',
			exchanges: [
				{exchangeId: 'okcoincn', apiKey: okcoincn_api.key, apiSecret: okcoincn_api.secret, tradeType: 'spot', tradePair: 'BTC-CNY'},
				{exchangeId: 'bitstamp', apiKey: bitstamp_api.key, apiSecret: bitstamp_api.secret, tradeType: 'spot', tradePair: 'BTC-USD', clientId: '930862'}
			]
		};

		manager.initBot(spotArbitrageConfig);

		// Bots.find().forEach(function (n) {
		// 	manager.initBot({
		// 		_id: n._id,
		// 		strategy: n.strategy,
		// 		exchanges: _.map(n.exchanges, function(m) {
		// 			var api = APIs.findOne({owner: n.owner, exchangeId: m.exchangeId});
		// 			return {exchangeId: m.exchangeId, apiKey: api.key, apiSecret: api.secret, tradeType: m.tradeType, tradePair: m.tradePair};
		// 		}),
		// 	});
		// });
	});
});
