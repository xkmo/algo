Meteor.publish('strategies', function() {
  var that = this;

  if (!that.userId) {
    that.ready();
    return;
  }

  CashCow.strategies.forEach(function(el) {
    that.added('strategies', el._id, el);
  });

  that.ready();
});

Meteor.publish('exchanges', function() {
  var that = this;

  if (!that.userId) {
    that.ready();
    return;
  }

  CashCow.exchanges.forEach(function(el) {
    that.added('exchanges', el._id, el);
  });

  that.ready();
});

Meteor.publish('apis', function() {
  if (!this.userId) {
    this.ready();
    return;
  }

  return APIs.find({owner: this.userId});
});

Meteor.publish('bots', function() {
  if (!this.userId) {
    this.ready();
    return;
  }

  return Bots.find({});
});

Meteor.publishComposite("items", function() {
  return {
    find: function() {
      if (!this.userId) {
        this.ready();
        return;
      }
      return Items.find({});
    }
    // ,
    // children: [
    //   {
    //     find: function(item) {
    //       return [];
    //     }
    //   }
    // ]
  }
});
