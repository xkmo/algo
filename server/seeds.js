Meteor.startup(function() {

  Factory.define('item', Items, {
    name: function() { return Fake.sentence(); },
    rating: function() { return _.random(1, 5); }
  });

  if (Items.find({}).count() === 0) {

    _(10).times(function(n) {
      Factory.create('item');
    });

  }

  // if (!Exchanges.findOne({name: 'okcoincn'})) {
  //   Exchanges.insert({name: 'okcoincn', displayName:'OKCoin China', homepage: 'https://www.okcoin.cn', pairs:[{base: "BTC", counter:"CNY"}, {base:"LTC", counter:"CNY"}]});
  //   Exchanges.insert({name: 'okcoincom', displayName:'OKCoin Int\'l', homepage: 'https://www.okcoin.com', pairs:[{base: "BTC", counter:"USD"}, {base:"LTC", counter:"USD"}]});
  //   Exchanges.insert({name: 'huobi', displayName:'Huobi', homepage: 'https://www.huobi.com', pairs:[{base: "BTC", counter:"CNY"}, {base:"LTC", counter:"CNY"}]});
  // }
});
