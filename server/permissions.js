Items.allow({
  'insert': function(userId, doc) {
    return userId;
  },
  'update': function(userId, doc, fields, modifier) {
    return userId;
  },
  'remove': function(userId, doc) {
    return userId;
  }
});

/* Need collection-hooks to check if data satisfy schema type.
  SimpleScema only validate undefined property.
 */

APIs.allow({
  insert: function(userId, doc) {
    return (userId && doc.owner === userId);
  },
  update: function(userId, doc, fields, modifier) {
    return (userId && doc.owner === userId);
  }
});

Bots.allow({
  insert: function(userId, doc) {
    return (userId && doc.owner === userId);
  },
  remove: function(userId, doc) {
    return (userId && doc.owner === userId);
  }
});

// Meteor.users.allow({
//   update: function (userId, doc, fields, modifier) {
//     return (userId && doc._id === userId);
//   }
// });
