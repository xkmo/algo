APIs = new Mongo.Collection('apis');

Schemas.API = new SimpleSchema({
	key: {
		type: String
	},
	secret: {
		type: String
	},
	updated: {
		type: Date
	},
	created: {
		type: Date
	},
	exchangeId: {
		type: String
	},
	owner: {
		type: String
	}
});

APIs.attachSchema(Schemas.API);
