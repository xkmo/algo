Exchanges = new Mongo.Collection('exchanges');

Schemas.Pair = new SimpleSchema({
	base: {
		type: String
	},
	counter: {
		type: String
	}
});

Schemas.TradeType = new SimpleSchema({
	type: {
		type: String
	},
	name: {
		type: String
	},
	pairs: {
		type: [Schemas.Pair]
	},
	leverage: {
		type: Boolean,
		optional: true
	},
	contracts: {
		type: [String],
		optional: true
	}
});

Schemas.Exchange = new SimpleSchema({
	name: {
		type: String
	},
	homepage: {
		type: String,
		regEx: SimpleSchema.RegEx.Url
	},
	tradeTypes: {
		type: [Schemas.TradeType]
	}
});

Exchanges.attachSchema(Schemas.Exchange);
