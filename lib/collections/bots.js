Bots = new Mongo.Collection('bots');

Schemas.ExchangeConfig = new SimpleSchema({
	exchangeId: {
		type: String
	},
	tradeType: {
		type: String
	},
	tradePair: {
		type: String // use Pair schema here
	}
});

Schemas.Bot = new SimpleSchema({
	strategy: {
		type: String
	},
	exchanges: {
		type: [Schemas.ExchangeConfig]
	},
	maxCapital: {
		type: Number,
		optional: true
	},
	margin: {
		type: Number,
		optional: true
	},
	owner: {
		type: String
	}
	// trades,
});

Bots.attachSchema(Schemas.Bot);
