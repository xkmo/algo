// Overide Meteor.call to check authorization
var originCall = Meteor.call;
Meteor.call = function() {
	if (!Meteor.user()) throw new Meteor.Error("not-authorized");

	// need validation function for server code, hide implementation

	originCall.apply(this, arguments);
}

Meteor.methods({
	addAPI: function (exchangeId, key, secret) {
		var now = new Date();
		APIs.insert(
			{key:key, secret: secret, updated: now, created: now, exchangeId: exchangeId, owner: Meteor.userId()}
			// function(err, _id) {
			// 	if (err) throw err;
			//
			// 	CashCow.manager.initExchange(APIs.findOne(_id));
			// }
		);
	},
	// updateAPI:

	addBot: function(bot) {
		Bots.insert(bot);
	},

	removeBot: function(_id) {
		Bots.remove(_id);
	}
});
