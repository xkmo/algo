Template.exchanges.onCreated(function () {
	this.subscribe("exchanges");
	this.subscribe('apis');
});

Template.exchanges.helpers({
	exchanges: function() {
		return Exchanges.find();
	},
	editingAPI: function() {
		var exchangeId = Session.get('editingExchange');
		var api = APIs.findOne({exchangeId: exchangeId});
		return api ? api : {key: '', secret: '', exchangeId: exchangeId};
	},
	editingExchange: function() {
		// this refer to the data context of editing API
		return Exchanges.findOne(this.exchangeId);
	}
});

Template.exchanges.rendered = function() { // onRendered won't called
	// var exchange = null;

	// maybe change to 'Template.bots.events'
	$('#edit-exchange-modal').on('show.bs.modal', function (event) {
		Session.set('editingExchange', $(event.relatedTarget).data('id'));

	  // var _id = $(event.relatedTarget).data('id');
		// exchange = Exchanges.findOne(_id);
		//
		// $(this).find('.modal-header .modal-title').text(exchange.name);
		//
		// var apis = Meteor.user().profile.apis;
		// var api = apis ? apis[_id] : null;
		// $('#apiKey').val(api ? api.key : '');
		// $('#apiSecret').val(api ? api.secret: '');
	})
	.find('.modal-footer .btn-primary').click(function (event) {
		var key = $('#apiKey').val();
		var secret = $('#apiSecret').val();
		var now = new Date();

		var exchangeId = Session.get('editingExchange');
		var api = APIs.findOne({exchangeId: exchangeId});

		if (api) {
			APIs.update(api._id, {$set: {key: key, secret: secret, updated: now}});
		}
		else {
			APIs.insert({key: key, secret: secret, updated: now, created: now, exchangeId: exchangeId, owner: Meteor.userId()});
		}
		// Meteor.call("addAPI", exchange._id, $('#apiKey').val(), $('#apiSecret').val());
	});
};
