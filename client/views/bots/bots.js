Template.bots.onCreated(function () {
  this.subscribe("bots");
  this.subscribe('apis');
  this.subscribe('strategies');
	this.subscribe("exchanges");
});

Template.bots.helpers({
  bots: function() {
    return Bots.find();
  },
  strategies: function() {
    return Strategies.find();
  },
	strategyIs: function (strategy) {
    return Session.get("selectedStrategy") === strategy;
  }
});
Template.bots.events({
	'change #add-bot-modal .strategy select': function(e) {
		Session.set("selectedStrategy", e.target.value);
    // reset following Session state based on e.target.value
    // Session.set("selectedExchange", undefined);
		// Session.set("selectedTradeType", undefined);
		// Session.set("selectedTradePair", undefined);
	},
	'click #add-bot-modal .modal-footer .btn-primary': function (e) {
    Bots.insert({
			strategy: Session.get("selectedStrategy"),
			exchanges: [{
        exchangeId: Session.get('selectedExchange'),
        tradeType: Session.get('selectedTradeType'),
        tradePair: Session.get('selectedTradePair')
      }],
			// maxCapital: $('#add-bot-modal .max-capital input').val(),
			// margin: $('#add-bot-modal .profit-margin input').val(),
      owner: Meteor.userId()
		});

    $('#add-bot-modal').modal('hide');
	},
  'hidden.bs.modal': function(e) {
    $('#add-bot-modal .modal-body select').val('placeholder');

    Session.set("selectedStrategy", undefined);
		Session.set("selectedExchange", undefined);
		Session.set("selectedTradeType", undefined);
		Session.set("selectedTradePair", undefined);
  }
});

Template.scalping.helpers({
  exchanges: function() {
    var exchangeIds = _.pluck(APIs.find({}, {fields: {exchangeId: 1}}).fetch(), 'exchangeId');
    return Exchanges.find({_id: {$in: exchangeIds}});
	},
  selectedExchange: function() {
    return Exchanges.findOne({_id: Session.get('selectedExchange')});
  },
  selectedTradeType: function() {
    // execute in context of selectedExchange
    return _.find(this.tradeTypes, {type: Session.get('selectedTradeType')});
  },
  selectedTradePair: function() {
    return Session.get('selectedTradePair');
  }
});
Template.scalping.events({
  'change .exchange select': function(e) {
		Session.set("selectedExchange", e.target.value);
	},
  'change .trade-type select': function(e) {
		Session.set("selectedTradeType", e.target.value);
	},
  'change .trade-pair select': function(e) {
		Session.set("selectedTradePair", e.target.value);
	}
});
